# This Dockerfile uses GitLab's AWS CLI based container & adds eksctl, kubectl, helm, terraform & GitLab's Terraform extensions to it.
# eksctl is a bit redundant as clusters can be created be created with Terraform. Also, eksctl is pinned to AWS EKS ONLY.

FROM registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

LABEL maintainer="kelly@mayadata.io"
LABEL version="20201122"
LABEL description="GitLab AWS tools + eksctl + kubectl + helm + Terraform + GitLab Terraform tools"

RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp \
  && mv /tmp/eksctl /usr/local/bin

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
  && chmod u+x kubectl \
  && mv kubectl /usr/local/bin

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 \
  && chmod 700 get_helm.sh \
  && ./get_helm.sh

RUN apt install -yq wget python3 unzip && \
    export latest_version=`curl https://checkpoint-api.hashicorp.com/v1/check/terraform | python3 -mjson.tool | grep current_version | awk '{print $2}' | sed s/\"\//g | sed s/,//g` && \
    echo $latest_version && \
    cd /tmp && \
    wget "https://releases.hashicorp.com/terraform/${latest_version}/terraform_${latest_version}_linux_amd64.zip" && \
    unzip *.zip && \
    rm -rf *.zip && \
    mv terraform /usr/local/bin && \
    wget https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh && \
    mv gitlab-terraform.sh /usr/local/bin/gitlab-terraform && \
    chmod u+x /usr/local/bin/gitlab-terraform && \
    apt remove -yq unzip wget python3 && \
    apt autoremove -yq && \
    cd .. && \
    rm -rf *.zip
