resource "random_string" "random" {
  length           = 7
  upper            = false
  lower            = true
  number           = true
  special          = false
  override_special = "/@\" "
}
