# Cluster Operations Project

The goal of this project is: 
1. build a container that provides AWS tools, `eksctl`, `terraform` and `kubectl` to jobs in the pipeline
2. Build an EKS cluster using `eksctl` 
3. Run a chaos test & note the results
4. Manually destroy an EKS cluster put up in step #2



